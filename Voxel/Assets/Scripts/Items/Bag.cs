﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voxel.Items
{
    public class Bag : Item
    {
        private int slots;

        [SerializeField]
        private GameObject bagPrefab;

        public UIBag bagScript { get; set; }

        public int Slots { get => slots;}

        public void Initialize(int slots) => this.slots = slots;
    } 
}
