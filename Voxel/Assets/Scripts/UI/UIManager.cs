﻿// To use this example, attach this script to an empty GameObject.
// Create three buttons (Create>UI>Button). Next, select your
// empty GameObject in the Hierarchy and click and drag each of your
// Buttons from the Hierarchy to the Your First Button, Your Second Button
// and Your Third Button fields in the Inspector.
// Click each Button in Play Mode to output their message to the console.
// Note that click means press down and then release.

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace Voxel.UI
{
    public class UIManager : MonoBehaviour
    {
        //Make sure to attach these Buttons in the Inspector
        public Button QuitButton;

        public GameObject PauseMenu;

        public GameObject HUD;

        bool Paused = false;

        private bool inventoryEnabled;

        [SerializeField] 
        private GameObject Inventory;

        [SerializeField]
        private Button[] actionButtons;

        private KeyCode action1, action2, action3;

        void Start()
        {
            QuitButton.onClick.AddListener(QuitGame);  

            PauseMenu.gameObject.SetActive(false);
            HUD.gameObject.SetActive(true);
            Inventory.gameObject.SetActive(false);

            //Keybinds
            action1 = KeyCode.Alpha1;
            action2 = KeyCode.Alpha2;
            action3 = KeyCode.Alpha3;
        }

        void Update()
        {
            //Open Pause Screen
            if (Keyboard.current.escapeKey.wasPressedThisFrame)
            {
                if (Paused == true)
                {
                    Time.timeScale = 1.0f;
                    PauseMenu.gameObject.SetActive(false);
                    HUD.gameObject.SetActive(true);
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    Paused = false;
                    Debug.Log("Menu Closed");
                }
                else
                {
                    Time.timeScale = 0.0f;
                    PauseMenu.gameObject.SetActive(true);
                    HUD.gameObject.SetActive(false);
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.Confined;
                    Paused = true;
                    Debug.Log("Menu Open");
                }
            }

            //Open Inventory Screen
            if (Keyboard.current.iKey.wasPressedThisFrame)

            {
                inventoryEnabled = !inventoryEnabled;

                if (inventoryEnabled == true)
                {
                    Inventory.SetActive(true);
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.Confined;
                }
                else
                {
                    Inventory.SetActive(false);
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            }

            //Action Button 1 pressed
            if (Input.GetKeyDown(action1))
            {
                ActionButtonOnClick(0);
            }

            //Action Button 2 pressed
            if (Input.GetKeyDown(action2))
            {
                ActionButtonOnClick(1);
            }

            //Action Button 3 pressed
            if (Input.GetKeyDown(action3))
            {
                ActionButtonOnClick(2);
            }
        }

        private void ActionButtonOnClick(int buttonIndex)
        {
            actionButtons[buttonIndex].onClick.Invoke();
        }

        private void TaskOnClick()
        {
            //Output this to console when Button1 or Button3 is clicked
            Debug.Log("You have clicked the button!");
        }

        private void TaskWithParameters(string message)
        {
            //Output this to console when the Button2 is clicked
            Debug.Log(message);
        }

        private void ButtonClicked(int buttonNo)
        {
            //Output this to console when the Button3 is clicked
            Debug.Log("Button clicked = " + buttonNo);
        }

        private void QuitGame()
        {
            Application.Quit();
            Debug.Log("Game is exiting");
        }

        public void Resume()
        {
            Time.timeScale = 1.0f;
            PauseMenu.gameObject.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            //Camera.GetComponent<AudioSource>().Play ();
        }
    }
}