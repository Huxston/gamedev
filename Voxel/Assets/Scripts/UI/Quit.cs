﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
     
    public class Quit : MonoBehaviour
    {
        void Update() 
        {
            if (Input.GetButtonDown("Quit")) 
            {
                QuitGame();
            }
        }

        void QuitGame()
        {
            Application.Quit();
            Debug.Log("Game is exiting");
        }
    }
