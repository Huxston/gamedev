﻿using UnityEngine;

namespace Voxel.Core
{
    public class Initialize : MonoBehaviour
    {
        void Start() => Time.timeScale = 1.0f;
    }
}
