﻿using UnityEngine;

namespace Voxel.Combat
{
    public class Chase : MonoBehaviour
    {
        public Transform Target;
        
        [SerializeField] 
        private int MoveSpeed = 4;
        
        [SerializeField] 
        private int MaxDist = 10;
        
        [SerializeField] 
        private int MinDist = 5;

        void Start()
        {
            Target = GameObject.Find("Third Person (Male)").transform;
        }

        void Update()
        {
            if (Target == null)
            {
                Target = GameObject.Find("Third Person (Male)").transform;
            }
            else
            {
                transform.LookAt(Target);

                if (Vector3.Distance(transform.position, Target.position) >= MinDist)
                {
                    transform.position += transform.forward * MoveSpeed * Time.deltaTime;

                    if (Vector3.Distance(transform.position, Target.position) <= MaxDist)
                    {
                        //Debug.Log("Attacking");
                    }
                }
            }
        }
    }
}