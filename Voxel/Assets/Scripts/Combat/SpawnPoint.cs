﻿using UnityEngine;

namespace Voxel.Combat
{
    public class SpawnPoint : MonoBehaviour
    {
        [SerializeField] 
        private int xMin = 1;
        
        [SerializeField] 
        private int xMax = 5;

        [SerializeField] 
        private int zMin = 1;
        
        [SerializeField] 
        private int zMax = 2;
        
        [SerializeField] 
        private int spawnLimit = 6;
                
        [SerializeField] 
        private float spawnCooldown = 1;

        private float timeUntilSpawn = 0;
        
        public GameObject Enemy;
        
        private int activeSpawn;
        
        public int ActiveSpawn { get => activeSpawn; set => activeSpawn = value; }

        void Start()
        {
            for (int i = 0; i < spawnLimit; i++)
            {
                Spawn();
                activeSpawn = activeSpawn + 1;
            }
        }

        void Update()
        {
            timeUntilSpawn -= Time.deltaTime;
            if (timeUntilSpawn <= 0)
            {
                if (ActiveSpawn < spawnLimit)
                {
                    Spawn(); // Spawn entity
                    activeSpawn = activeSpawn + 1;
                }
                // Reset for next spawn
                timeUntilSpawn = spawnCooldown;
            }
        }

        private void Spawn()
        {
            Vector3 newPos = new Vector3(Random.Range(this.transform.position.x + xMin, this.transform.position.x + xMax), this.transform.position.y, Random.Range(this.transform.position.z + zMin, this.transform.position.z + zMax));
            GameObject octo = Instantiate(Enemy, newPos, Quaternion.identity) as GameObject;
        }
    }
}

