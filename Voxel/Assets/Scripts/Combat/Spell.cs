﻿using UnityEngine;

public class Spell : MonoBehaviour
{
    private Rigidbody myRigidBody;

    [SerializeField]
    private float speed;

    public Transform MyTarget { get; set; }

    void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate() 
    {
        Vector3 direction = MyTarget.position - transform.position;

        myRigidBody.velocity = direction.normalized * speed;
    }
}
