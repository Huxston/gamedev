﻿using UnityEngine;

namespace Voxel.Combat
{
    public class DestroyEntity : MonoBehaviour
    {
        private void Destroy(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                other.gameObject.SetActive(false);
            }
        }
    }
}
