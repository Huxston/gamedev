﻿namespace Voxel.Control
{
    public enum State
    {
        Idle,
        Walking,
        Attacking,
        Dead,
        Sleeping
    } 
}
