﻿using UnityEngine;

namespace Voxel.Control
{
    public abstract class Entity : MonoBehaviour
    {
        protected State state;

        protected Coroutine attackRoutine;

        protected virtual void Start()
        {

        }

        protected virtual void Update() 
        {

        }

        public void StopAttack()
        {
            if (attackRoutine != null)
            {
                StopCoroutine(attackRoutine);
                state = State.Idle;
                // myAnimator.SetBool("attack", state);
            }
        }
    }
}
