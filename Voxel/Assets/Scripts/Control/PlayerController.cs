﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Voxel.UI;

namespace Voxel.Control
{
    public class PlayerController : Entity
    {
        [SerializeField]
        private Stat health;

        [SerializeField]
        private Stat mana;

        [SerializeField]
        private float defaultHealth;

        [SerializeField]
        private float defaultMana;

        [SerializeField]
        private GameObject[] spellPrefab;

        [SerializeField]
        private Transform exitPoint;

        private Transform target; // only for debugging

        protected override void Start()
        {
            health.Initialize(defaultHealth, defaultHealth);
            mana.Initialize(defaultMana, defaultMana);
        }

        protected override void Update()
        {
            GetInput();

            InLineOfSight();

            FindEnemy();
        }

        private void FindEnemy()
        {
            target = GameObject.FindGameObjectWithTag("Enemy").transform; // Just for testing and debugging
        }

        private void GetInput()
        {
            //DEBUG ONLY
            if (Input.GetKeyDown(KeyCode.N))
            {
                health.MyCurrentValue -= 10;
                mana.MyCurrentValue -= 10;
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                health.MyCurrentValue += 10;
                mana.MyCurrentValue += 10;
            }//

            if (Keyboard.current.cKey.wasPressedThisFrame)
            {
                if (state == State.Idle)
                {
                }
            }
        }

        private IEnumerator Attack(int spellIndex)
        {
            state = State.Attacking; // Set entity state to attacking

            // Not Implemented: Attack Animation
            // myAnimator.SetBool("attack", state);
            yield return new WaitForSeconds(1); // Hard-coded cast time for debugging

            Spell s = Instantiate(spellPrefab[spellIndex], exitPoint.position, Quaternion.identity).GetComponent<Spell>();

            s.MyTarget = target;


            StopAttack();
        }

        public void CastSpell(int spellIndex)
        {
            attackRoutine = StartCoroutine(Attack(spellIndex));
        }

        private bool InLineOfSight()
        {
            //Vector3 targetDirection = (target.transform.position - transform.position).normalized;

            //GameObject hitTarget;

            //RaycastHit hit = Physics.Raycast(exitPoint.position, targetDirection, out hitTarget, Vector3.Distance(exitPoint.position, target.transform.position));

            //hitTarget = hit.collider.gameObject;

            return false;
        }
    }
}
